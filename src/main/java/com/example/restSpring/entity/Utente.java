package com.example.restSpring.entity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="utente")
//@EntityListeners(AuditingEntityListener.class)
//@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class Utente implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    private String nome;

    @NotBlank
    private String cognome;

    @OneToMany(mappedBy="utente")
    private List<Cellulare> cellulari;

    public List<Cellulare> getCellulari() {
        return cellulari;
    }

    public void setCellulari(List<Cellulare> cellulari) {
        this.cellulari = cellulari;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
}


//}
//    // Getters and Setters ... (Omitted for brevity)
//
//    private Date updatedAt;
//    @LastModifiedDate
//    @Temporal(TemporalType.TIMESTAMP)
//    @Column(nullable = false)
//
//    private Date createdAt;
//    @CreatedDate
//    @Temporal(TemporalType.TIMESTAMP)
//    @Column(nullable = false, updatable = false)
//
//    private String content;
//    @NotBlank
//
//    private String title;
//    @NotBlank
//
//    private Long id;
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Id
//public class Note implements Serializable {
//        allowGetters = true)
//@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
//@EntityListeners(AuditingEntityListener.class)
//@Table(name = "notes")
//@entity
//
//import java.util.Date;
//import javax.persistence.*;
//
//import org.springframework.data.jpa.domain.support.AuditingEntityListener;
//import org.springframework.data.annotation.LastModifiedDate;
//import org.springframework.data.annotation.CreatedDate;
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//import org.hibernate.validator.constraints.NotBlank;
//
//package com.example.easynotes.model;

package com.example.restSpring.repository;

import com.example.restSpring.entity.Cellulare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CellulareRepo extends JpaRepository<Cellulare, Long> {

}
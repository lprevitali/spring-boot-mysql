package com.example.restSpring.controller;


import com.example.restSpring.entity.Cellulare;
import com.example.restSpring.repository.CellulareRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CellulareController {

    @Autowired
    CellulareRepo cellulareRepository;

    @PostMapping("/phones")
    public Cellulare newPhone(@Valid @RequestBody Cellulare cellulare){
        return cellulareRepository.save(cellulare);
    }

    @GetMapping("/phones")
    public List<Cellulare> getPhones(){
        return cellulareRepository.findAll();
    }

    @GetMapping("/phones/{id}")
    public ResponseEntity<Cellulare> getPhone(@PathVariable(value= "id") Long id){
        Cellulare cellulare = cellulareRepository.findOne(id);
        if(id == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(cellulare);
    }

    @PutMapping("/phones/{id}")
    public ResponseEntity<Cellulare> updatePhone(@PathVariable(value= "id") Long id, @RequestBody Cellulare cellulareUpdate){
        Cellulare cellulare = cellulareRepository.findOne(id);
        if(id == null) {
            return ResponseEntity.notFound().build();
        }
        cellulare.setModello(cellulareUpdate.getModello());
        cellulare.setNumero(cellulareUpdate.getNumero());
        cellulare.setNumero(cellulareUpdate.getNumero());
//        cellulare.setUtente(cellulareUpdate.getUtente());

        Cellulare cellulareUpdated = cellulareRepository.save(cellulare);

        return ResponseEntity.ok().body(cellulareUpdated);
    }

    @DeleteMapping("/phones/{id}")
    public ResponseEntity<Cellulare> deletePhone(@PathVariable(value= "id") Long id){
        Cellulare cellulare = cellulareRepository.findOne(id);
        if(id == null) {
            return ResponseEntity.notFound().build();
        }
        cellulareRepository.delete(id);
        return ResponseEntity.ok().body(cellulare);
    }
}

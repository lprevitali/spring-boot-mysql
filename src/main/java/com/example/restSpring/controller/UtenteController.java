package com.example.restSpring.controller;


import com.example.restSpring.entity.Utente;
import com.example.restSpring.repository.UtenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UtenteController {

    @Autowired
    UtenteRepository utenteRepository;

    @PostMapping("/users")
    public Utente newUser(@Valid @RequestBody Utente utente){
        return utenteRepository.save(utente);
    }

    @GetMapping("/users")
    public List<Utente> getUsers(){
        return utenteRepository.findAll();
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Utente> getUser(@PathVariable(value= "id") Long id){
        Utente utente = utenteRepository.findOne(id);
        if(id == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(utente);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Utente> updateUser(@PathVariable(value= "id") Long id, @RequestBody Utente utenteUpdate){
        Utente utente = utenteRepository.findOne(id);
        if(id == null) {
            return ResponseEntity.notFound().build();
        }
        utente.setNome(utenteUpdate.getNome());
        utente.setCognome(utenteUpdate.getCognome());

        Utente utenteUpdated = utenteRepository.save(utente);

        return ResponseEntity.ok().body(utenteUpdated);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Utente> deleteUser(@PathVariable(value= "id") Long id){
        Utente utente = utenteRepository.findOne(id);
        if(id == null) {
            return ResponseEntity.notFound().build();
        }
        utenteRepository.delete(id);
        return ResponseEntity.ok().body(utente);
    }
}
